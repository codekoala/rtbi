package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"strings"

	"github.com/jackc/pgx/v5"
	jsoniter "github.com/json-iterator/go"
	nats "github.com/nats-io/nats.go"
	"github.com/nats-io/nats.go/jetstream"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type (
	DebeziumEvent struct {
		Payload Payload `json:"payload"`
		// Schema Schema `json:"schema"`
	}

	Payload struct {
		After       map[string]any `json:"after"`
		Before      map[string]any `json:"before"`
		Op          string         `json:"op"`
		Source      Source         `json:"source"`
		Transaction any            `json:"transaction"`
		TsMs        int64          `json:"ts_ms"`
	}

	Schema struct {
		Fields   []map[string]any `json:"fields"`
		Name     string           `json:"name"`
		Optional bool             `json:"optional"`
		Type     string           `json:"type"`
		Version  int64            `json:"version"`
	}

	Source struct {
		Connector string `json:"connector"`
		Db        string `json:"db"`
		LSN       int64  `json:"lsn"`
		Name      string `json:"name"`
		Schema    string `json:"schema"`
		Sequence  string `json:"sequence"`
		Snapshot  string `json:"snapshot"`
		Table     string `json:"table"`
		TsMs      int64  `json:"ts_ms"`
		TxID      int64  `json:"txId"`
		Version   string `json:"version"`
		Xmin      any    `json:"xmin"`
	}
)

var json = jsoniter.ConfigCompatibleWithStandardLibrary

func main() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	ctx := context.Background()

	pc, err := pgx.Connect(ctx, os.Getenv("PGURI"))
	if err != nil {
		log.Fatal().Err(err).Msg("failed to connect to postgres")
	}
	defer pc.Close(ctx)

	nc, err := nats.Connect("nats://127.0.0.1:4422")
	if err != nil {
		log.Fatal().Err(err).Msg("failed to connect to NATS")
	}

	js, err := jetstream.New(nc)
	if err != nil {
		log.Fatal().Err(err).Msg("failed to connect to jetstream")
	}

	stream, err := js.Stream(ctx, "DebeziumStream")
	if err != nil {
		log.Fatal().Err(err).Msg("failed to create stream")
	}
	log.Info().Any("stream", stream).Msg("have stream")

	consumer, err := stream.Consumer(ctx, "ParallelConsumer")
	if err != nil {
		log.Error().Err(err).Msg("failed to get consumer")
		consumer, err = js.CreateConsumer(ctx, "DebeziumStream", jetstream.ConsumerConfig{
			Durable:   "ParallelConsumer",
			AckPolicy: jetstream.AckExplicitPolicy,
		})
		if err != nil {
			log.Fatal().Err(err).Msg("failed to create consumer")
		}
	}

	cc, err := consumer.Consume(func(msg jetstream.Msg) {
		msg.Ack()

		// var payload map[string]any
		var evt DebeziumEvent

		if err := json.Unmarshal(msg.Data(), &evt); err != nil {
			log.Error().Err(err).Msg("failed to decode payload")
			return
		}

		log.Info().Any("subject", msg.Subject()).Any("data", evt).Msg("received msg ")

		var sql string
		switch evt.Payload.Op {
		case "c":
			idx := 1
			cols := []string{}
			phs := []string{}
			vals := []any{}
			for key, val := range evt.Payload.After {
				if key == "id" {
					continue
				}
				cols = append(cols, fmt.Sprintf(`"%s"`, key))
				phs = append(phs, fmt.Sprintf("$%d", idx))
				vals = append(vals, val)
				idx++
			}

			sql = fmt.Sprintf(
				`
                INSERT INTO "%s"."%s" (%s)
                VALUES (%s)
                `,
				evt.Payload.Source.Schema,
				evt.Payload.Source.Table,
				strings.Join(cols, ", "),
				strings.Join(phs, ", "),
			)
			log.Info().Str("query", sql).Any("vals", vals).Msg("have query")
			if _, err = pc.Exec(ctx, sql, vals...); err != nil {
				log.Error().Err(err).Msg("failed to delete record")
			}
		case "u":
			idx := 1
			cols := []string{}
			vals := []any{}
			for key, val := range evt.Payload.After {
				if key == "id" {
					continue
				}
				cols = append(cols, fmt.Sprintf(`"%s" = $%d`, key, idx))
				vals = append(vals, val)
				idx++
			}

			vals = append(vals, evt.Payload.After["id"])
			sql = fmt.Sprintf(
				`
                UPDATE "%s"."%s"
                SET %s
                WHERE "id" = $%d
                `,
				evt.Payload.Source.Schema,
				evt.Payload.Source.Table,
				strings.Join(cols, ", "),
				idx,
			)
			log.Info().Str("query", sql).Any("vals", vals).Msg("have query")
			if _, err = pc.Exec(ctx, sql, vals...); err != nil {
				log.Error().Err(err).Msg("failed to delete record")
			}
		case "d":
			sql = fmt.Sprintf(
				`
                DELETE FROM "%s"."%s" WHERE "id" = $1
                `,
				evt.Payload.Source.Schema,
				evt.Payload.Source.Table,
			)
			id := evt.Payload.Before["id"]
			log.Info().Str("query", sql).Any("id", id).Msg("have query")
			if _, err = pc.Exec(ctx, sql, id); err != nil {
				log.Error().Err(err).Msg("failed to delete record")
			}
		}
	})
	if err != nil {
		log.Fatal().Err(err).Msg("failed to consume")
	}
	defer cc.Stop()

	log.Info().Msg("waiting for 🪓")

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, os.Kill)
	<-sig

	log.Info().Msg("all done")
}
