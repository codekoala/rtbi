table "books" {
  schema = schema.public
  column "id" {
    null = false
    type = serial
  }
  column "title" {
    null = true
    type = text
  }
  column "author" {
    null = true
    type = text
  }
  primary_key {
    columns = [column.id]
  }
}
table "profile" {
  schema = schema.public
  column "id" {
    null = false
    type = serial
  }
  column "name" {
    null = true
    type = text
  }
  column "color" {
    null = true
    type = text
  }
  primary_key {
    columns = [column.id]
  }
}
schema "public" {
  comment = "standard public schema"
}
